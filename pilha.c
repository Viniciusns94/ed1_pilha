#include <stdio.h>
#include <stdlib.h>

#define MAX 10
typedef struct {
	int topo;
	int itens[MAX] ;
} pilha;

void inicia_pilha(pilha *p) {
	p->topo = -1;
}

int pilha_vazia(pilha p) {
	return p.topo == -1;
}

int pilha_cheia(pilha p) {
	return p.topo == MAX-1;
}

void empilha(pilha *p, int v) {
	if(pilha_cheia(*p))
		puts("Pilha Cheia");
	else
		p->itens[++p->topo] = v;
}

int desempilha(pilha *p) {
	if(pilha_vazia(*p) )
		puts("Pilha Vazia") ;
	else
		return p->itens[p->topo--];
}

int mostra(pilha *p){
	int i;
	for(i=0;i<p->topo+1;i++){
		printf(" %d",p->itens[i]);
	}
	printf("\n");
}

void main(){
	pilha P;
	inicia_pilha(&P);
	printf("Empilha: ");
	empilha(&P,0);
	empilha(&P,1);
	empilha(&P,2);
	empilha(&P,3);
	empilha(&P,4);
	empilha(&P,5);
	empilha(&P,6);
	empilha(&P,7);
	empilha(&P,8);
	empilha(&P,9);
	mostra(&P);
	desempilha(&P);
	printf("Desempilha: ");
	mostra(&P);
}